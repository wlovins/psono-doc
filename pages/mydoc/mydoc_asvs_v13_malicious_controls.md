---
title: V13 Malicious controls
tags: [asvs, owasp]
summary: "Malicious controls verification requirements"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v13_malicious_controls.html
folder: mydoc
---

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     | 
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------| 
| 13.1                        | Verify all malicious activity is adequately sandboxed, containerized or isolated to delay and deter attackers from attacking other applications.                                                                                                                                                                                                               |                         |            | x           | 2.0       | 
| 13.2                        | Verify that the application source code, and as many third party libraries as possible, does not contain back doors, Easter eggs, and logic flaws in authentication, access control, input validation, and the business logic of high value transactions.                                                                                                      |                         |            | x           | 3.0.1     | 

