---
title: V12 Security configuration
tags: [asvs, owasp]
summary: "Security configuration verification requirements"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v12_security_configuration.html
folder: mydoc
---

This section was incorporated into V11 in Application Security Verification Standard 2.0. 

