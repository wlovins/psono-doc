---
title: Healthchecks
tags: [server]
summary: "A brief description of the implemented healthcheck"
sidebar: mydoc_sidebar
permalink: mydoc_other_healthcheck.html
folder: mydoc
---

## Healtcheck

To monitor the health of your own installation, Psono provides a health check ressource.

<span class="label label-default">GET</span> /healthcheck/

It will (in case of a success) return a HTTP status code 200 and a JSON of the following format:

```json
{
    "time_sync":{
        "healthy":true
    },
    "db_read":{
        "healthy":true
    },
    "db_sync":{
        "healthy":true
    }
}
```

Any other HTTP status code is considered "unhealthy".